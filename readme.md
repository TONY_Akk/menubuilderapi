# MenuBuilderAPI

MenuBuilderAPI 是一款方便的,在游戏中创建Menu的API,依赖于TabooLib

## 如何导入?

### Maven

首先导入仓库:

```xml
<repository>
  <id>MaxMC-Repo</id>
  <url>http://play.maxmc.cc:7994/releases/</url>
</repository>
```

或

```xml
<repository>
  <id>YUMC-Repo</id>
  <url>https://repo.yumc.pw/repository/maven-releases/</url>
</repository>
```

然后添加依赖:

```xml
<dependency>
  <groupId>cn.maxmc.menubuilderlib</groupId>
  <artifactId>MenuBuilderLib</artifactId>
  <version>2.0.0</version>
</dependency>
```

### Gradle

添加仓库:

```groovy
maven {
    url "http://play.maxmc.cc:7994/releases/"
}
```

或

```groovy
maven {
    url "https://repo.yumc.pw/repository/maven-releases/"
}
```

然后添加依赖

```groovy
implementation 'cn.maxmc.menubuilderlib:MenuBuilderLib:2.0.0'
```



