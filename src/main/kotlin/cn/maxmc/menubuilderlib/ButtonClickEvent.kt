package cn.maxmc.menubuilderlib

import org.bukkit.entity.Player
import org.bukkit.event.inventory.ClickType
import org.bukkit.event.inventory.InventoryAction
import org.bukkit.event.inventory.InventoryType

data class ButtonClickEvent(
    val player: Player,
    val session: MenuSession,
    val slot: Int,
    val action: InventoryAction,
    val click: ClickType,
    val slotType: InventoryType.SlotType
)