package cn.maxmc.menubuilderlib

import cn.maxmc.menubuilderlib.component.IButton
import io.izzel.taboolib.kotlin.Reflex.Companion.reflexMethods
import io.izzel.taboolib.kotlin.Reflex.Companion.staticInvoke
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.HandlerList
import org.bukkit.event.inventory.InventoryType
import org.bukkit.inventory.Inventory
import org.bukkit.plugin.Plugin
import org.bukkit.scheduler.BukkitTask

class Menu(
    plugin: Plugin,
    val name: String,
    val type: InventoryType? = null,
    val size: Int = 6 * 9,
    update: Boolean = false,
    updateDelay: Long = 20,
    val close: (Player) -> Unit = {},
    val interactive: Boolean = false
) {
    val cacheMap = HashMap<Inventory, MenuSession>()
    private val content = HashMap<Int, IButton>()
    private lateinit var task: BukkitTask
    private val listener = MenuListener(this)
    var stoped = false
    init {
        Bukkit.getPluginManager().registerEvents(listener, plugin)
        if (update) {
            task = Bukkit.getScheduler().runTaskTimer(plugin, {
                cacheMap.forEach { _, session ->
                    session.update()
                }
            }, 0L, updateDelay)
        }
    }

    fun setButton(slot: Int, button: IButton) {
        content[slot] = button
    }

    @Suppress("UNCHECKED_CAST")
    fun open(player: Player): MenuSession {
        val inv = createInventory(player)
        val session = MenuSession(player, this, inv, content.clone() as HashMap<Int, IButton>)
        cacheMap[inv] = session
        player.openInventory(inv)
        return session
    }

    fun stopMenu() {
        if (stoped) {
            return
        }
        stoped = true
        cacheMap.forEach { _, session ->
            session.close()
        }

        if (this::task.isInitialized) {
            task.cancel()
        }

        MenuListener::class.java.reflexMethods().forEach { (_, method) ->
            if (!method.isAnnotationPresent(EventHandler::class.java)) {
                return@forEach
            }
            val handlerList = method.parameterTypes[0].staticInvoke<HandlerList>("getHandlerList")!!
            handlerList.unregister(listener)
        }
    }

    private fun createInventory(player: Player): Inventory {
        val inventory = if (type == null) {
            Bukkit.createInventory(player, size, name)
        } else {
            Bukkit.createInventory(player, type, name)
        }
        content.forEach { slot, button ->
            inventory.setItem(slot, button.getIcon(player).replacePlaceholderAPI(player))
        }
        return inventory
    }

}