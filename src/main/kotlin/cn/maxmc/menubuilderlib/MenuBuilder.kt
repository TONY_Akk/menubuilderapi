package cn.maxmc.menubuilderlib

import cn.maxmc.menubuilderlib.component.EmptyButton
import cn.maxmc.menubuilderlib.component.IButton
import io.izzel.taboolib.util.Ref
import org.bukkit.entity.Player
import org.bukkit.event.inventory.InventoryType
import org.bukkit.plugin.Plugin
import java.util.logging.Logger

/**
 * 菜单创建工具
 * @since 2021.3.28
 *
 * @author TONY_All
 */
class MenuBuilder private constructor(val plugin: Plugin) {
    private lateinit var name: String
    private var type: InventoryType? = null
    private var size: Int = 0
    private var updateDelay: Long = -1
    private var update: Boolean = false
    private var interactive = false;
    private var close: (Player) -> Unit = { }
    private val content = HashMap<Int, IButton>()

    /**
     * 设置菜单的名字(即向玩家展示的Title)
     *
     * @param name 菜单的Title
     *
     * @return 当前MenuBuilder
     *
     */
    fun name(name: String): MenuBuilder {
        this.name = name
        return this
    }

    /**
     * 设置菜单GUI类型
     *
     * **如果设置了Type, 则size将不起作用!**
     *
     * @param type 菜单GUI类型
     *
     * @return 当前MenuBuilder
     */
    fun type(type: InventoryType): MenuBuilder {
        this.type = type
        return this
    }

    /**
     * 如不设置改项,MenuBuilder会根据菜单内容自动适配大小.
     *
     * 菜单大小默认不超过6*9(54)格,
     *
     * 如需更大的菜单,请将系统属性`big_chest`设置为`allow`
     *
     * **如果设置了type,请不要修改size!**
     *
     * @param size Menu的大小
     *
     * @return 当前MenuBuilder
     */
    fun size(size: Int): MenuBuilder {
        if (type != null) {
            throw IllegalArgumentException("Typed can not set size.")
        }

        if (size > 6 * 9) {
            warnLargeSize()
        }

        this.size = size
        return this
    }

    fun close(close: (Player) -> Unit): MenuBuilder {
        this.close = close
        return this
    }

    /**
     * 添加按钮.
     *
     * @param slot 按钮在物品栏中为位置.
     * @param button 按钮对象
     *
     * @return 当前MenuBuilder
     */
    fun button(slot: Int, button: IButton): MenuBuilder {
        content[slot] = button
        return this
    }

    /**
     * 批量设置菜单中按钮
     *
     * Button在数组的位置即为在菜单中的位置
     *
     * 可以使用 EmptyButton 表示空位
     *
     * @see EmptyButton
     *
     * @param button 按钮
     *
     * @return 当前MenuBuilder
     */
    fun buttons(vararg button: IButton): MenuBuilder {
        if (button.size > 6 * 9) {
            warnLargeSize()
        }

        button.forEachIndexed { index, iButton ->
            content[index] = iButton
        }
        return this
    }

    /**
     * 批量设置菜单中按钮
     *
     * 方便 Kotlin 使用 `slot to button` 的语法进行设置
     *
     * @param button 按钮以及所在的slot
     *
     * @return 当前MenuBuilder
     */
    fun buttons(vararg button: Pair<Int, IButton>): MenuBuilder {
        val slots = button.map { it.first }.toList()
        if (slots.maxOrNull() ?: 0 > (6 * 9)-1) {
            warnLargeSize()
        }

        button.forEach { (slot, iButton) ->
            content[slot] = iButton
        }

        return this
    }

    /**
     * 设置菜单允许交互
     *
     * 默认: 禁止交互
     */
    fun interactive(): MenuBuilder {
        this.interactive = true
        return this
    }

    /**
     * 设置菜单禁止交互 (指禁止点击物品栏)
     *
     * 默认: 禁止交互
     */
    fun unInteractive(): MenuBuilder {
        this.interactive = true
        return this
    }

    /**
     * 设置菜单是否更新以及更新的延迟
     * 将delay设置为小于0的值禁止菜单更新
     *
     * 默认: 不更新
     *
     * @param delay 更新延迟
     * @return 当前MenuBuilder
     */
    fun update(delay: Long): MenuBuilder {
        if (delay > 0) {
            update = true
            updateDelay = delay
        } else {
            update = false
        }
        return this
    }

    private fun warnLargeSize() {
        if (System.getProperty("big_chest", "deny") != "allow") {
            Logger.getLogger("Minecraft").warning("Menu larger than 54 is not recommended.")
            Logger.getLogger("Minecraft").warning("If you don't want to see this, set \"big_chest\" to \"allow\"")
        }
    }

    /**
     * 构建菜单.
     *
     * @return 构建好的Menu对象
     */
    fun build(): Menu {
        // analyze size
        val size = if (this.size == 0) {
            (content.keys.maxOrNull() ?: 1) / 9 + 1
        } else {
            ((this.size-1) / 9) + 1
        }

        val menu = Menu(plugin, name, type, size * 9, update, updateDelay, close, interactive)
        for (i in 0 until size*9) {
            content[i] ?: EmptyButton.let { content[i] = it }

            menu.setButton(i, content[i]!!)
        }

        return menu
    }

    companion object {
        @JvmStatic
        fun start(name: String): MenuBuilder {
            val plugin = Ref.getCallerPlugin()
            return MenuBuilder(plugin).name(name)
        }

        @JvmStatic
        fun start(plugin: Plugin, name: String): MenuBuilder {
            return MenuBuilder(plugin).name(name)
        }
    }
}