package cn.maxmc.menubuilderlib

import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryCloseEvent

class MenuListener(private val menu: Menu) : Listener {
    @EventHandler
    fun onClose(e: InventoryCloseEvent) {
        if (menu.cacheMap[e.inventory] != null) {
            menu.cacheMap.remove(e.inventory)
            menu.close(e.player as Player)
        }
    }

    @EventHandler
    fun onClick(e: InventoryClickEvent) {
        menu.cacheMap[e.whoClicked.openInventory.topInventory].also {
            if (it != null && !it.menu.interactive) {
                e.isCancelled = true
            }
        }
        val session = menu.cacheMap[e.clickedInventory] ?: return
        val clickEvent = ButtonClickEvent(e.whoClicked as Player, session, e.slot, e.action, e.click, e.slotType)
        val button = session.getButton(e.slot) ?: return e.setCancelled(true)
        e.isCancelled = !button.onClick(clickEvent)
    }
}