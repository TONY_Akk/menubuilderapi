package cn.maxmc.menubuilderlib

import cn.maxmc.menubuilderlib.component.IButton
import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory

class MenuSession(
    val player: Player,
    val menu: Menu,
    val inventory: Inventory,
    private val content: MutableMap<Int, IButton>
) {
    val context = HashMap<String, Any>()

    fun updateContent(vararg button: Pair<Int, IButton>) {
        button.forEach { (slot, iButton) ->
            content[slot] = iButton
        }
        update()
    }

    fun getButton(slot: Int): IButton? {
        return content[slot]
    }

    fun update() {
        content.forEach { slot, button ->
            inventory.setItem(slot, button.getIcon(player).replacePlaceholderAPI(player))
        }
    }

    fun clear() {
        content.clear()
    }

    fun close() {
        player.closeInventory()
        menu.cacheMap.remove(inventory)
    }

}