package cn.maxmc.menubuilderlib

import io.izzel.taboolib.module.compat.PlaceholderHook
import io.izzel.taboolib.util.item.ItemBuilder
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.inventory.ItemStack

val isHooked by lazy { PlaceholderHook.isHooked() }

fun ItemStack.replacePlaceholderAPI(sender: CommandSender = Bukkit.getConsoleSender()): ItemStack {
    if (!isHooked) {
        return this
    }
    val meta = this.itemMeta?: return this
    val name = meta.displayName
    val lore = this.itemMeta.lore?: emptyList()
    val loreReplaced = ArrayList<String>()
    lore.forEach { loreReplaced.add(PlaceholderHook.replace(sender, it)) }
    val builder = ItemBuilder(this)
    if (name != null) {
        val nameReplaced = PlaceholderHook.replace(sender, name)
        builder.name(nameReplaced)
    }
    builder.lore(loreReplaced)
    return builder.build()
}