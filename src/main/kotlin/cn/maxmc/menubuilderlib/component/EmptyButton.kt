package cn.maxmc.menubuilderlib.component

import cn.maxmc.menubuilderlib.ButtonClickEvent
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

object EmptyButton : IButton {
    override fun getIcon(player: Player): ItemStack {
        return ItemStack(Material.AIR)
    }

    override fun onClick(event: ButtonClickEvent): Boolean {
        return true
    }
}