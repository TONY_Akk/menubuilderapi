package cn.maxmc.menubuilderlib.component

import cn.maxmc.menubuilderlib.ButtonClickEvent
import io.izzel.taboolib.util.item.ItemBuilder
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

open class FunctionalButton(val icon: ItemBuilder, val function: (ButtonClickEvent) -> Unit) : UnmovableButton() {
    override fun onClick0(event: ButtonClickEvent) {
        function(event)
    }

    override fun getIcon(player: Player): ItemStack {
        return icon.build()
    }
}