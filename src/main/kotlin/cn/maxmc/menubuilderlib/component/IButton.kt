package cn.maxmc.menubuilderlib.component

import cn.maxmc.menubuilderlib.ButtonClickEvent
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack

interface IButton {
    fun getIcon(player: Player): ItemStack

    fun onClick(event: ButtonClickEvent): Boolean
}