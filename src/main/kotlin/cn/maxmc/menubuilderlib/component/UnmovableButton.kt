package cn.maxmc.menubuilderlib.component

import cn.maxmc.menubuilderlib.ButtonClickEvent

abstract class UnmovableButton : IButton {
    override fun onClick(event: ButtonClickEvent): Boolean {
        onClick0(event)
        return false
    }

    abstract fun onClick0(event: ButtonClickEvent)
}