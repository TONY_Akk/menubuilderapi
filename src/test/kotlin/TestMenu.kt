import cn.maxmc.menubuilderlib.MenuBuilder
import cn.maxmc.menubuilderlib.component.EmptyButton

class TestMenu {

    fun createMenu() {
        val menu = MenuBuilder.start("Test")
            .buttons(2 to EmptyButton)
            .size(6*9)
            .build()
    }

}